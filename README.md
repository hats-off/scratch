# Scratch

#### Options

* **selector** - (*string*) - change this value according to your HTML
* **width** - (*integer*) - set canvas width
* **height** - (*integer*) - set canvas height
* **image** - (*string*) - set sratch image path
* **brush** - (*string*) - set brush image path
* **fullColor** - (*boolean*) - set true or false whether you want to use image or color as scratch element
* **color** - (*string*) - set canvas hex color

#### Methods

* **stop** - stops the scratching

#### Listeners:

* **scratching** - use listener to track the scratch progress

```javascript
window.addEventListener('scratching', function(){
	console.log(scratch.progress);
}, false);
```