/***************************
*
*	Scratch
*
*	---------------
*		Options:
*	---------------
*
*	selector - change this value according to your HTML
* 	width - set canvas width
* 	height - set canvas height
* 	image - set sratch image path
* 	brush - set brush image path
* 	fullColor - set true or false whether you want to use image or color as scratch element
*	color - set canvas hex color
*
*	---------------
*		Methods:
*	---------------
*
*	stop - stop scratching

*	---------------
*		Listeners:
*	---------------
*
*	scratching - trigger at scratching 
*
***************************/


(function(global, factory) {
	if (typeof define === 'function' && define.amd) {
		define(function() {
			return factory(global, global.document);
		});
	} else if (typeof module !== 'undefined' && module.exports) {
		module.exports = factory(global, global.document);
	} else {
		global.Scratch = factory(global, global.document);
	}
} (typeof window !== 'undefined' ? window : this, function (window, document) {
	
	'use strict';

	function Scratch(params) {
		
		// default options
		var options = {
			selector : '#scratch',
			width : 200,
			height : 100,
			image : 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAMgAAABkCAIAAABM5OhcAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyJpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMy1jMDExIDY2LjE0NTY2MSwgMjAxMi8wMi8wNi0xNDo1NjoyNyAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENTNiAoV2luZG93cykiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6MDY2OEZBMjJDNEU0MTFFOEIzMTRGMzU2MDRBQjUwRTMiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6MDY2OEZBMjNDNEU0MTFFOEIzMTRGMzU2MDRBQjUwRTMiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDowNjY4RkEyMEM0RTQxMUU4QjMxNEYzNTYwNEFCNTBFMyIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDowNjY4RkEyMUM0RTQxMUU4QjMxNEYzNTYwNEFCNTBFMyIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/Phkh3nEAAA+DSURBVHja7J1bk1TFEkZ7YA6iIldBQmFEDV544cH/yf8ElavAAY6MK3oFX6S1d/fsnmmPPmQ+TOxL7aqsrK+y8lLVc/Do0aPVanVwcLBa03Bx4t/pxfT5iQ+Xf7W9rRP/zl7M3m6i4+PjTbe5Hi5mb2f/Lnm4/PmuH57i75aLc6umpr+BGlhNDaymBlZTA6upqYHV1MBqamA1NTWwmhpYTQ2spqYGVlMDq6mB1dTUwGpqYDU1sJqaGlhNDaymBlZTUwOrqYHV1MBqampgNTWwmhpYTU0NrKYGVlMDq6mpgdXUwGpqYDU1NbCaGlhNDaymptPQ4U6lj4+P//jjj/99Im4PDg7OnTt3eHj4nzVxkR8FtfzHjx/55MOHD5Tnmifnz5+n2IULF/yEzynPcwpQkjKrv/6yqAX8ir/eVn4kb+sPk1qeW97mxzZnf4PU+mGGv1zDAwy/e/fu/fv31lz7CIW9SMOuWTKVTJ9P5UMZ2vq4ployXVMgQ818UjuVixTjospz+K3RiDSc2JZjOvtbo+fXtPCXWncDFiwi6Ldv3yJx8fHFF1/Y9uvXr2kSrPDks88+U3ZK7b9r4oKSX331lYzy5M2bN5a/ePEiQ0WvqMSS09+uVVgUo/Dnn38uHBXHmzVZfwWK/MAMLcKw+Fht+BFbC1+6dInK6SYVwgyDxyseWoaHVFJ5pmYYfvXqlQLJmNEob9+tqcKFyqmNGtIubP/++++0JQKcGICPGrjlc6FpYeq070pvAEEQwOcUox7KiL/V5JdnFanyEeuMLOVrR+pfCkTyewaWw4MU4PX27dvXr1+Hs4gDgjNETJ8pDBPyKlbA040bNy5fvpwZnPLAVGkqR6pl2OpPEQejdJtbKqTkl19+iVD8ivq5vXr16vCVsFZeVEuZ6MIpwlSrTnG4ohUQ8N1338F5dIk8v3jxggp5Ap9IgCfU8/3336ub6/TjbX1OzbA6THr5RzjUpgZSDggZHmBboKu93q+JzxVUXlVgUYymlTMMHB0dyX8g4ifUI+5pxUlCi3ZkKO9fh/7cmgL0/QBLjvl7//59+Hi7proaIjIGG44zEgwAZb799tuvv/4azp49e+YEUgmr8Bz7aHXr8XbQWODSeaPmUHC2dWFNPsxXvqIMn1ggWm34ge6MOnNXdcXMgWfKZ5DCCWzANlLmuUpLFSU0HQO+unLlChCkUZ+Lcp7zeUYuw+Yi66gLLD50vB3y6H51dlZVZ8J0mCgDS7x1TczESDEFAjTpCJ9YTDlnJgzAEs15uzdgaQpQNddPnz51pXD4AyzIYUA9aKOoqBAxHci6cP4TIRoZTSV8pVKMGovRIFlDzCzlxTXSFNM+V0A84UL4Zu2Y/SF7C8AqTcMz3XQFd2bLs+uvNcO5PPi5a5bA5SuYuXnzJgVc2fNcjTX8Ujw12LoTTAABC8Ch7WW/lCdoUOcpqJiAwc2HNdFlF1yGSf2nnCVFLf/yFjMXba1eHIBlQ/R6wPF+gEUbYtz5qoZgsUCOgsDu0RM4QDoU4C19UwTOXeQV/aT6VZqaBbFY7U9UumZ15h+1UT5TXCVXNVYuKAlKXq+J2rTBgwnt6xjgMgOw6IKY4IIhVyH5lbzBuRNDU8+uKfSqbuO7RG1scYnShUwnu5+HURUKKo1WodkXcVMZy6usMNbjQ2YUfRHH6YgM179q9D2HG6hRtazERQZjcOfOHfTT1FPjgrFXM2sFUwOc/fjjj9hD1TNyxmeQqrjtM60MHoqqKMNsh2ND1M6r57WNwLdwr28BHNNUO5cK6aPAdTgdpHv37rG0aeSGB6qK//XPkoJyMjiB1WrTf6LBq7t37zJPgs6M19Qqj8fq3ON2ueW+g8YSCokyxLN1gYvGis+v2qSf1XlBP9t/9VbGuE6ykb+1j6NSzEMH1elFncefSB7Sf2UHD9h5olBVP/jYDx8+lG0HJh5JzBGXIXgOanVN/j3Agufba5Jz5Pn8+fPZwppfUWaRg778UC30888/x65CPpuU7pmAVfW56lREa0xUg1QVonqo+sM+q7qqYreSKbCCjBhwwVDCSH5VgZV6nAwBN/zo7g1cxTpGq03t3KwXWnj1K6fW7AxcPgBbRL08aBS3Ois7OniWB9hGILpZNQQzO6sh3XYKK6IhrLMHYDmusX6UKdwbdGEeJPRnTMi4wyCsambWJ1vCS7ylbwOwqvSNVAVYWsc1uBrbRdOwOsxyawG9ObXddEimTMZxGW2LtRmnIocZ8bcTquwCzGBjLF9MDOklYjfbrnNYpW6B9GvaER9qySRQsv8AKcICtrJVXSqa1AlHCoaktYH0LGb7ZvhHUzFr6JRpRaz/j/OfKJEBizjGdaolrJ8Fy7ZWi/+510ZTdK2eZdth26RRDKOgGpldm9TAdpQYpDAiukRDRFAsf05CF7vZYhgDmpLVet6EGPjX93KthJYv/TtoLKPJ1ZBK5uTFmtBbhg11jzfJHcGpPKrzP+XYmrkAtTRtvEB3GlM6ntFgYwVP8aszNqdenuANO89wrrVFO85qLPuYENSuwDKspbqqS8T2T5yfOkxVsJUxxXLt2rU49Y4CzvtUEQhEEySacTut8ofLp6zxdCAc20LOtHBNesCfEbzZvvkhInNps5J4lFNXNiktw1TmHHl48+ZNfX7tuQosWg/ovWAynMXosRK9DVNYg7M8+4kTYHBRl7eoL+ZwLuFcNOteOMMNCk7XCpfpWGOUx+k27DwNsBn7hUw6BWF7zhUerokO6HGIngSlzCurVLb7paaoku8TVUMgoAbE9b9s8fr160w4pVbtmCEeMURBNb3PshrSu6tXr1a1Gm98k5s2OAE7Aau62MsjDk5sxIJGh1VVwBaGGQgcSVDFvB1s4nySQLFpgGmsfz+7Gxgb06jPnj1L0DYaNVDbAiy1jgtlAu4acAPHcSENkAprij158sTIGZXUdTD4rlo9Af2zAMsOVs2XDQhTG0gzQD13ikYT7Zx1Obd/EsdtGt9PLxIgdbB+/fVX00fTjiRYb/0aWPuPvMt0khvffPMNdtLLly+juuKo07aL3exs4y2gNKNcLZipvWmL6Aleod5TWDVmrtqmh+iA4YyaajRSfwpTOhUyAPQ3Zpw8081pH9Uczi6d5V2BZRJGEzsdOXFoDI0m+ThVpTJ/dHT0yy+/JFZiVGjWz8hGphqb3OSTnSkJDQca76oQxIqtw5APal/HLQ7tdN5Apv1l1+zspsltgJShilWnn2j4vkYH1Hwmv+tehsQXzqKxwnO2KqgFDYJMFcODBw/00XY175TerVu3cFkeP368ZD0dAqSqzE1xLFPyNVejtmP2DqBJgNT+UpLuLDcZd9vd4HAm30kzqC7WpqE9c4KbQikJFjgkFLNmUHtigDQzqdpn1b2gKoAYY2gvEYfsXorpKrgZfpfI2XCRueQhGLuwOfWW/o3ByeUBUtXMlKsESCFhlFCWmmJ7gDT7FvcMLKcCtrMrC9esg8jOpco8SfoPx3iws7ano2tKWIWvLKa5wgrTZONr5J2/LqAJrRnCMQlTo6AxB5eEvDdNBmOPw1aF2ZWuZtZPgaosiLXXSwbINbRuqZ0tVpMT2/uevEXd77D/lA7cmPRwQTQzIIycmnVR2M56NHASyTWNvVyJusylw/EincExsxaGGTOoW5y17G7YKeXyt1JWAFfAiOV0s2gokD3Aqs/9B0jlXmWrpslOHfVT/FWfa7xP94oMykP1Y8h++YRIdFQHOPsaHO94kYP9vl1jJZdSYxZKcwg52vQSs/r/Bix9GjwMVi7jCDulX7ZU664Ql5SLa1qoAnYIkCZbV9d1/TU0lot63XufnXqpgbeUd+BT3nzfKYI91RuNusJsHzCaVNp251+uagJfP4i/uLFKOft/3D56lmj+foEln0zva9euaQ07zc4OLDqIi6axpbG7UAWcWw4sd8JU20UAYb/XcAMXly5dMiqRzfwJoxtpjKlkhR5Y2NUcUS3XTUXU8MMPP8BPsparsuFnO7BM0lky8W7qcRcvAxb9qpKG52Hm/LPYSk91hmTvjNXGwnMPvtJYWO1uKR0jDqu/Hp758Imy0dEgk3szsiVmWl6znYc3btzQIdhpKXTLeT1NYOhfDpM+S8bwROPd0IaTQVZlO2dDjIxgx3CLHzOYlf8g1T0tynw5ArY7BI64ky0nEvZsY8VwAbzZk15TvA4kmpPp4tmEy5cvP3/+XJvMDHzsM7+lzP3799UTDn9NGmjdIyPPnGXbjCaO2cmcWDLGkfhy3UBhQzpx2Xbs82qDc2GolrVPc7UeptOHd4/hTz/95PJqv9xy49Ij/2mrPs/+5mH9dUekGaps38imyJxPiXfsJ7yK81snmJrbzdwmeRI9NzPh86oIMov01qeHKXJYYXmQ+XAnfWu2DuljKul8VTExrnfv3gVV+r0uJbdu3YJd4JWtm/nkypUrR0dHhmo8guHBnrqRwzyoe4jraRmVk8nRVGh43YTx06dPBX0y3AZ1nzx5YmSobqyoW94QvYFfeE7lWbuxY+7cuaPeNedNedrKvo/Vp62OvHr8+HHVxI5NNX0oRoX0+uXLl7Ff5U18v3r1qh4MFAF8Am8KZDj+VTe+Ig06W3dH5jnVJiiYPC/Pf/vtt5gB079bQkIzaHn06NFq7rTdcGp0VY4X51iSp+fsMMrJPRseIK4HbFTLpo2thM/dwF+3LzsXc4Synndwe4xaKjaTGeh4Z8lauvjWY9nxnLW0cp4ijmod/izZq/VmfL1dNRMjKs8ul+mjecxh179KMYGueoK3Dl7O4tYxswkVcwwde+2kNWAb5786swlfG2Kt5mZOFGZQavRbjyTpnWkWXEvAmT80N704zRH77EYyi549Ku4Iq2aHoteb8CBvPSGTE4K18mTKqqqooYq6czfjl5FL1iXLRMRXD1zU+ofNLXWLMyj3+FAqN0w6SHYIr9f9zbXkcNhmEOnweUL8dbTqkRsjqFONFcMgp+aHOFxO58YmSXxO6E93MSWRVcMC+9zdUAUR3VAHewg2Vrk7C4dXQ2QyUdP62w3DHvD6avY2Q+jYDOvvdGvN9LYOzxA4HSJzFcqb5u6m22ncdbr61PVxAFDWsimwclFV4+yiOdwOdW75u39gDaKf/obHiXuMBtys/q20CQerpiVhhBZBUwOrqYHV1MBqampgNTWwmhpYTU0NrKYGVlMDq6mpgdXUwGpqYDU1NbCaGlhNDaympgZWUwOrqYHV1NTAampgNTWwmpoaWE0NrKYGVlNTA6upgdXUwGpqamA1NbCaGlhNTQ2spgZWUwOrqekU9KcAAwDIBtR6lpNeXgAAAABJRU5ErkJggg==',
			brush : 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAFAAAAAxCAYAAABNuS5SAAAKFklEQVR42u2aCXCcdRnG997NJtlkk83VJE3apEma9CQlNAR60UqrGSqW4PQSO9iiTkE8BxWtlGMqYCtYrLRQtfVGMoJaGRFliijaViwiWgQpyCEdraI1QLXG52V+n/5nzd3ENnX/M8/sJvvt933/533e81ufL7MyK7NOzuXPUDD0FQCZlVn/+xUUQhkXHny8M2TxGsq48MBjXdAhL9/7YN26dd5nI5aVRrvEc0GFEBNKhbDjwsHh3qP/FJK1EdYIedOFlFAOgREhPlICifZDYoBjTna3LYe4xcI4oSpNcf6RvHjuAJRoVszD0qFBGmgMChipZGFxbqzQkJWVZUSOF7JRX3S4LtLTeyMtkkqljMBkPzHRs2aYY5PcZH/qLY1EIo18byQ6hBytIr3WCAXcV4tQHYvFxg3w3N6+Bh3OQolEoqCoqCinlw16JzTFJSE6PYuZKqvztbC2ex7bzGxhKu+rerjJrEEq+r9ieElJSXFDQ0Mh9zYzOzu7FBUWcO4Q9xbD6HYvhXhGLccVD5ZAPyfMqaioyOrBUgEv8FZXV8caGxtz8vLykhCWTnZIKmsKhUJnEYeKcKk2YYERH41G7UYnck1/WvAPOxsdLJm2+bEY0Ay0RNeqkytXQkoBZM4U5oOaoYSUkBGRtvnesrBZK4e4F6ypqSkuLy+v4KI99ZQxkfc6vZ4jNAl1wkbhG8LrhfNBCdkxmhYacvj/GOce+3K9MHHbDHUmicOufREELRIWch/DljzMsglutr+VIJO5KjGrVfZAnpF8mnCd8G5hrnC60Cl8T/iw8C1hKd9P9eDCMcgo5HwBx8BB/g7xeRPkrBbeJ3xTeAxjvRGVV3NcshfPG1JX4tVDQae47GuVOknCi23xHr5nyrxe2C1sFlYJ7xe+Jlwm7BRulItP0ms957RzTMK1ws41jMS8eDxehopaOCYfxc3AIHcIX+K6nxW+ImyVF1i8PQ8DTuwtdC1atCja3NwcHkq5EuXmo85G+jq+yMm28V4q/zcIPxV+K9zPxnbgTi0ocybu6wX66fx/vfAB4T1gHt8xI1wlXMF5zEXnQKC56ruEjwhvEa4WrrXvK/Yt5Pt5I1UveeVKyKmT+lpG2gQ2npMmez8ZzFT3e+HXwj7hKXNf6rFZbDpJUjESLdFsFX4mfFv4Fd/7qPBm4UPCJ4RNwncwym4UfYVUtiAcDk/T+3NRmylwWzAY7BCBCwYYogZPnrJoRNm2IDc3tw4FVKXFm95UmGLzkTTFpog524WnhQPCQeGvwiPCCuFCYmk5GbEJt3tOeF54HPVeLLyXxHOv8BPhYaFLeFU4gsI7OWeZk3g+hpJNvVMGIIqhdRvy+biVISouq2TBqWxoIL1wgBhU5AR1SzJvFR4UnhX+Bl4RfsFGP0npUkTymIQ7fh8Cf4l6F0LgXkj6o3O+buGfwj+ElzGQETaNeJqPhxiahckYq8KJ9V6mP+4pTIATjsGCA8lCQVy9VbhB2CM8itu9IBxlkx6O4nbmmpcSi0KUExa3Psfn23DZC4lhlhRuIWs/R1Y9BrpR4WHcfiOq34bLl5DJm1B7BANPGO4+2OJfDcVwX+RZkL5d+DRqeRJ360IJx1CFp4w/8/lhVGXxay1xKp8asQ31rSbgz2az1aBBWCZsgKTfEFe7uM4xYus9KHWXcBv3eolwJe67hJLIN6yubMVpW1tbbllZWVxtzjRquvQe9981IG3RZHUQttH7hB8IP0cdLwp/YnNHcdsjEP1xsEruO56i2Fy3UWXMskAgYAH/EjOiCD6NDc/XZ4v12RqSy3WQ9rJD3jPClwkZz2Aoy8JnUEjPcwYWfgfHvcIW84h308mABQP4Xp02OY44M4tSZSfx7UXIewU3NpXuxw0vJzauYDP1XM8y8Ttx67fhylYrdlAMW1x7h/BF3NWI+4PwFwjbSha26/xQuBmib6HDqeI+m4m5wzrj9A/xO+O5qbm4yizcbDOKfAjVWeC/WzAFLSeI+4hN9WzQ65EvED7D8Tt4vwE33O64rIfD1JW3k6xeQoX3UN6chyG8In4tcbHuRAyKw2ktVIIM2U5XcA7t2FKy5vWQeBexbbrTpvmZiJwN6e3EwKspW/ajqBuAKfKQk8m7KIce5bgnMNQDkLWPUmkj511DSVV5HJOd417FzrDAK7RjZLMZiURigmLVFCYs5tI2PFhpcUj/n6z6sp72LwJKiU2rUdp62rA7IX4XytpJ3Weh4XfE1/0kk/uoFX8kbCHudZLld5E8vJIs2+mbT8iznaR60DHMBt0EE1DySVlSsOBvyrL6zkZG5qI2T/QSBYTHMYAlq2tw1+0MFO4kVj5GSbSbgvkA8fQQr1uIdfdD5mZ1GhZbP0XfuwlPmOp0SNkYbkQV2JdlEsq69VJS+rTER+NtZVC+TX+NRFq1XGeiHXbGUHMg6lk2/DiZ+mHU8wTueoTXLtS3F5e9l2PNZW9lyrOB5LGSmJokzMQ6OjqCA3wsMXLLhqrWoZgKe3lyZ5YtLiwsLLfMLhJL0ibW3rKa7oMQ+Ajq6gKHcMeHeP8qZcpRMvyt1J97SRabcNP1ZGsbKhSb6lF+5GR6shUnlqTSyPM7LZxV/PUqjOfTH6cvqx+XyN3aCfBPUWh3UZIcxC2/jgu/BJ7Eve/G1R/EXS9gaLCc0dgySqIm7jV4MhEYdAaN4R4eRHkBusJp3GNp56iSOscyYN0DaUch8Ai13X6yrg0PvotCO8nme0geKymBaulc1qO+NbxOOpHZtrcHR+nT6+wePvcnk8k8qv6iNBdyH4/OoGR5gXbv75D4NIX3NoruLSjtKmLlbTwCKER1NmV+QIqfS13aai0izUHsRKksAQE5g0w4fuehj9f+xb25Ym1tbcIhuw2COmkBn2cAcQAFbsclV1BTns49JZio3EQWPkgCySJpFIu8aor0UfeLigDTlUTa/8eimhRGuUiKOZPYtYNabh9EGik3Mkk+A9I8JTWoAiik/LEpzY8tY4uwWc4AJMjxQd8oXRHU8JqbW32orNyAiubZo0WR5wX9KyHrLpLD52nrxhFHa1CVV5w3081cRu/7BYichpEqfafA7/sCzhT7tVkhLZvhTeB8Gv1r6U+ty/gqtWHQCSNTcPOl9NmXM1S4hgRjBjjL1MdUJ8cx3uhe3d3dfh5Meb8qyKWsuJRidwtN/h20XEtxvTwya7tKncU8ACqmXVwLict5fy6TnFhra2uW7xT8dWk2BHptVBOx8GLKjo3g7bhrBQq1sdVsCvEkhLZIac1y/zmUSO0oO8fX/0P2Ub3cwaWpZSITnLnOpDlBWTIfMleJqFb10jXCBJUlMyORSIP14LhqNef6v/05bpZTdHulUyXKsufDNdRxZ4vIhSKwhQFG5vfLfcwZsx2X92Jhje8/P8OI+TK/oO+zeA84WTzkvI/6RuB3y6f68qf11xnyMiuzMms4178AwArmZmkkdGcAAAAASUVORK5CYII=',
			fullColor : true,
			color : '#ababab'
		};
		
		// merge default options with user's
		Object.assign(options, params);
		
		var self = this;
		
		var isDrawing = false;
		var lastPoint = null;
		var lastPixels = 0;
		
		self.progress = lastPixels;
		self.stop = function() {
			isDrawing = false;
		};

		var element = document.querySelector(options.selector);
		
		var canvas = document.createElement('canvas');
		canvas.setAttribute('width', options.width);
		canvas.setAttribute('height', options.height);
		canvas.setAttribute('class', 'scratch');
		element.appendChild(canvas);
		
		var ctx = canvas.getContext('2d');
		
		if (options.fullColor === true) {
			ctx.fillStyle = options.color;
			ctx.fillRect(0, 0, canvas.width, canvas.height);
		} else {
			var image = new Image();
			image.crossOrigin = 'anonymous';
			image.src = options.image;
			image.onload = function(){
				ctx.drawImage(image, 0, 0, canvas.width, canvas.height);
			};
		}
		
		// set scratch brush source (could be image url/path)
		var brush = new Image();
		brush.src = options.brush;
		
		
		// add mouse events
		canvas.addEventListener('mousedown', mouseDown, false);
		canvas.addEventListener('mouseup', mouseUp, false);
		canvas.addEventListener('mousemove', mouseMove, false);
		
		// create custom event
		if (typeof document.CustomEvent === 'function') {
			self.event = new document.CustomEvent('scratching', {
				bubbles: true,
				cancelable: true
			});
		} else if (typeof document.createEvent === 'function') {
			self.event = document.createEvent('Event');
			self.event.initEvent('scratching', true, true);
		} else {
			return false;
		}
		
		// stop scratching on mouse release
		function mouseUp(){
			isDrawing = false;
		}
		
		// start scratching when mouse is down
		function mouseDown(e){
			isDrawing = true;
			lastPoint = getMouse(e, canvas);

			ctx.globalCompositeOperation = 'destination-out';
			ctx.drawImage(brush, lastPoint.x - 25, lastPoint.y - 25);
		}
		
		// redraw canvas / scratch it on mouse move
		function mouseMove(e){
			if (!isDrawing) return;

			e.preventDefault();

			let currentPoint = getMouse(e, canvas);
			let dist = Distance(lastPoint, currentPoint);
			let angle = Angle(lastPoint, currentPoint);
			let x, y;
			
			for (var i = 0; i < dist; i++) {
				x = lastPoint.x + (Math.sin(angle) * i) - 25;
				y = lastPoint.y + (Math.cos(angle) * i) - 25;
				ctx.globalCompositeOperation = 'destination-out';
				ctx.drawImage(brush, x, y);
			}
			
			lastPoint = currentPoint;
			lastPixels = Pixels();
			self.progress = lastPixels;
			
			window.dispatchEvent(self.event);
		}
		
		// get canvas pixels
		 function Pixels(){
			let stride = 1;
			let p = ctx.getImageData(0, 0, canvas.width, canvas.height);
			let pdata = p.data;
			let l = pdata.length;
			let total = (l / stride);
			let count = 0;

			for (let i = 0; i < l; i += stride) {
				if (parseInt(pdata[i]) === 0) {
					count++;
				}
			}

			return Math.round((count / total) * 100);
		}
		
		// get distance between two points
		function Distance(point1, point2) {
			return Math.sqrt(Math.pow(point2.x - point1.x, 2) + Math.pow(point2.y - point1.y, 2));
		}
		
		// get angle between two points
		function Angle(point1, point2) {
			return Math.atan2(point2.x - point1.x, point2.y - point1.y);
		}

		// get mouse position
		function getMouse(e, canvas) {
			let offsetX = 0, offsetY = 0, mx, my;

			if (canvas.offsetParent !== undefined) {
				do {
					offsetX += canvas.offsetLeft;
					offsetY += canvas.offsetTop;
				} while ((canvas = canvas.offsetParent));
			}

			mx = (e.pageX || e.touches[0].clientX) - offsetX;
			my = (e.pageY || e.touches[0].clientY) - offsetY;

			return {
				x : mx,
				y : my
			};
		}
	}
	
	Scratch.prototype.handleEvent = function (e) {
		if (typeof (this[e.type]) === 'function') {
			return this[e.type](e);
		}
	};

	return Scratch;
	
}));